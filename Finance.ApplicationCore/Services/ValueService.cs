﻿using Finance.ApplicationCore.Entities.Value;
using Finance.ApplicationCore.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Finance.ApplicationCore.Services
{
	public class ValueService : IValueService
	{
		private readonly IAsyncRepository<Value> _valueRepository;
		private readonly ILogger<ValueService> _logger;

		public ValueService(IAsyncRepository<Value> valueRepository, ILogger<ValueService> logger)
		{
			_valueRepository = valueRepository;
			_logger = logger;
		}

		public async Task<Value[]> GetValues()
		{
			_logger.LogInformation("Получение Значений");

			var values = await _valueRepository.ListAllAsync() ?? new List<Value>();

			_logger.LogInformation($"Получены следующие Значения: {string.Join(',', values.Select(x => x.Name))}");

			return values.ToArray();
		}

		public async Task<Value> GetValueById(int id)
		{
			var value = await _valueRepository.GetByIdAsync(id);

			return value;
		}
	}
}
