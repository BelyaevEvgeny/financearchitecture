﻿namespace Finance.ApplicationCore.Entities
{
	public class BaseEntity
	{
		public int Id { get; set; }
	}
}
