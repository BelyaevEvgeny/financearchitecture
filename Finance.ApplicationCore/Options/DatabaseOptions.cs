﻿namespace Finance.ApplicationCore.Options
{
	public class DatabaseOptions
	{
		public bool UseInMemoryDatabase { get; set; }
		public string ConnectionString { get; set; }
	}
}
