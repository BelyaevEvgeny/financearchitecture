﻿using Finance.ApplicationCore.Entities.Value;
using System.Threading.Tasks;

namespace Finance.ApplicationCore.Interfaces
{
	public interface IValueService
	{
		Task<Value[]> GetValues();
		Task<Value> GetValueById(int id);
	}
}
