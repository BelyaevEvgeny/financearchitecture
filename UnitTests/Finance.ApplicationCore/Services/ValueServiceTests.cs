﻿using Finance.ApplicationCore.Entities.Value;
using Finance.ApplicationCore.Interfaces;
using Finance.ApplicationCore.Services;
using Microsoft.Extensions.Logging;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTests.Finance.ApplicationCore.Services
{
	public class ValueServiceTests
	{
		private Mock<IAsyncRepository<Value>> _valueRepository;

		public ValueServiceTests()
		{
			_valueRepository = new Mock<IAsyncRepository<Value>>();
		}

		[Fact]
		public async Task Should_InvokeValueRepository_ListAllAsync_Once()
		{
			var values = new Value[0];
			_valueRepository.Setup(x => x.ListAllAsync()).ReturnsAsync(values);

			var valueService = new ValueService(_valueRepository.Object, Mock.Of<ILogger<ValueService>>());

			await valueService.GetValues();

			_valueRepository.Verify(x => x.ListAllAsync(), Times.Once);
		}

		[Fact]
		public async Task Should_InvokeValueRepository_ListAllAsync_ReturnValues()
		{
			var values = new Value[1];
			values[0] = new Value() { Id = 1, Name = It.IsAny<string>() };
			_valueRepository.Setup(x => x.ListAllAsync()).ReturnsAsync(values);

			var valueService = new ValueService(_valueRepository.Object, Mock.Of<ILogger<ValueService>>());

			var resultValues = await valueService.GetValues();

			Assert.Equal(values, resultValues);
		}

		[Fact]
		public async Task Should_InvokeValueRepository_ListAllAsync_ReturnEmptyArray()
		{
			Value[] values = null;
			_valueRepository.Setup(x => x.ListAllAsync()).ReturnsAsync(values);

			var valueService = new ValueService(_valueRepository.Object, Mock.Of<ILogger<ValueService>>());

			var resultValues = await valueService.GetValues();

			Assert.Empty(resultValues);
		}
	}
}
