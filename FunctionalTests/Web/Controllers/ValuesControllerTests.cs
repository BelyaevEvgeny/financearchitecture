﻿using Finance.ApplicationCore.Entities.Value;
using Finance.WebApi;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace FunctionalTests.Web.Controllers
{
	public class ValuesControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
	{
		public ValuesControllerTests(CustomWebApplicationFactory<Startup> factory)
		{
			Client = factory.CreateClient();
		}

		public HttpClient Client { get; }

		[Fact]
		public async Task ReturnAllValues()
		{
			var httpResponse = await Client.GetAsync("/api/values");
			httpResponse.EnsureSuccessStatusCode();

			var stringResponse = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<IEnumerable<Value>>(stringResponse);

			Assert.NotEmpty(response);
		}

		[Fact]
		public async Task Return_Value_ById()
		{
			var expectedValue = new Value() { Id = 1, Name = "Test" }; ;
			var httpResponse = await Client.GetAsync($"/api/values/{expectedValue.Id}");
			httpResponse.EnsureSuccessStatusCode();

			var stringResponse = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<Value>(stringResponse);

			Assert.Equal(expectedValue.Id, response.Id);
			Assert.Equal(expectedValue.Name, response.Name);
		}

		[Fact]
		public async Task Return_Value_ById_Null()
		{
			var id = 2;
			var httpResponse = await Client.GetAsync($"/api/values/{id}");
			httpResponse.EnsureSuccessStatusCode();

			var stringResponse = await httpResponse.Content.ReadAsStringAsync();
			var response = JsonConvert.DeserializeObject<Value>(stringResponse);

			Assert.Null(response);
		}
	}
}
