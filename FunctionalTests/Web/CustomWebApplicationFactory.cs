﻿using Finance.Infrastructure.Data;
using Finance.WebApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace FunctionalTests.Web
{
	public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
	{
		protected override void ConfigureWebHost(IWebHostBuilder builder)
		{
			builder.ConfigureServices(services =>
			{
				services.AddEntityFrameworkInMemoryDatabase();

				// Create a new service provider.
				var provider = services
					.AddEntityFrameworkInMemoryDatabase()
					.BuildServiceProvider();

				// use in-memory database
				services.AddDbContext<FinanceContext>(c =>
				{
					c.UseInMemoryDatabase("InMemoryDbForTesting");
					c.UseInternalServiceProvider(provider);
				});
					

				// Build the service provider.
				var sp = services.BuildServiceProvider();

				// Create a scope to obtain a reference to the database
				// context (ApplicationDbContext).
				using (var scope = sp.CreateScope())
				{
					var scopedServices = scope.ServiceProvider;
					var db = scopedServices.GetRequiredService<FinanceContext>();
					var loggerFactory = scopedServices.GetRequiredService<ILoggerFactory>();

					var logger = scopedServices.GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

					// Ensure the database is created.
					db.Database.EnsureCreated();

					try
					{
						FinanceContextSeed.Seed(db);
					}
					catch (Exception ex)
					{
						logger.LogError(ex, $"An error occurred seeding the " +
							$"database with test messages. Error: {ex.Message}");
					}
				}
			});
		}
	}
}
