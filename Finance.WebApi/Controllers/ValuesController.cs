﻿using Finance.ApplicationCore.Entities.Value;
using Finance.ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Finance.WebApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ValuesController : ControllerBase
	{
		private readonly IValueService _valueService;

		public ValuesController(IValueService valueService)
		{
			_valueService = valueService;
		}

		/// <summary>
		/// Получение value1 и value2
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Value>>> Get()
		{
			return await _valueService.GetValues();
		}

		/// <summary>
		/// Получение value по id
		/// </summary>
		/// <remarks>
		/// Пример запроса:
		///
		///     GET /{id}
		///     {
		///        "id": 1
		///     }
		///
		/// </remarks>
		/// <param name="id">Любой int</param>
		/// <returns></returns>
		[HttpGet("{id}")]
		public async Task<ActionResult<Value>> Get(int id)
		{
			return await _valueService.GetValueById(id);
		}

		// POST api/values
		[HttpPost]
		public void Post([FromBody] string value)
		{
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public void Put(int id, [FromBody] string value)
		{
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}
	}
}
