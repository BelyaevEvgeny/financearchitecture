﻿using Finance.ApplicationCore.Common;
using Finance.ApplicationCore.Options;
using Finance.Infrastructure.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Finance.WebApi
{
	public class Program
	{
		public async static Task Main(string[] args)
		{
			var host = CreateWebHostBuilder(args).Build();

			await SeedDbAsync(host);

			host.Run();
		}

		private static async Task SeedDbAsync(IWebHost host)
		{
			var config = new ConfigurationBuilder()
					.SetBasePath(Directory.GetCurrentDirectory())
					.AddJsonFile("appsettings.json", optional: true)
					.Build();

			DatabaseOptions databaseOptions = config.GetSection(Const.Options.Database).Get<DatabaseOptions>();

			using (var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
				var loggerFactory = services.GetRequiredService<ILoggerFactory>();
				try
				{
					var financeContext = services.GetRequiredService<FinanceContext>();

					if (!databaseOptions.UseInMemoryDatabase)
						await FinanceContextSeed.RecreateDatabase(financeContext);

					await FinanceContextSeed.SeedAsync(financeContext);
				}
				catch (Exception ex)
				{
					var logger = loggerFactory.CreateLogger<Program>();
					logger.LogError(ex, "An error occurred seeding the DB.");
				}
			}
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.AddConsole();
					logging.AddDebug();
				});
	}
}
