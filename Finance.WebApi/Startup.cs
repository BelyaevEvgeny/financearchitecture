﻿using Finance.ApplicationCore.Common;
using Finance.ApplicationCore.Interfaces;
using Finance.ApplicationCore.Options;
using Finance.ApplicationCore.Services;
using Finance.Infrastructure.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace Finance.WebApi
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			// Register the Swagger generator, defining 1 or more Swagger documents
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
				
				// Set the comments path for the Swagger JSON and UI.
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);
			});

			services.AddScoped<IValueService, ValueService>();
			services.AddScoped(typeof(IAsyncRepository<>), typeof(EFRepository<>));

			// Настройка параметров и DI
			services.AddOptions();
			services.Configure<DatabaseOptions>(Configuration.GetSection(Const.Options.Database));

			DatabaseOptions databaseOptions = Configuration.GetSection(Const.Options.Database).Get<DatabaseOptions>();
			if (databaseOptions.UseInMemoryDatabase)
				ConfigureInMemoryDatabases(services);
			else
				ConfigureMsSqlDatabase(services, databaseOptions);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseMvc();

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			app.UseSwagger(); 

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
			});
		}

		private void ConfigureInMemoryDatabases(IServiceCollection services)
		{
			// use in-memory database
			services.AddDbContext<FinanceContext>(c =>
				c.UseInMemoryDatabase("Finance"));
		}

		private void ConfigureMsSqlDatabase(IServiceCollection services, DatabaseOptions databaseOptions)
		{
			services.AddDbContext<FinanceContext>(options => 
				options.UseSqlServer(databaseOptions.ConnectionString));
		}
	}
}
