﻿using Finance.ApplicationCore.Entities.Value;
using Finance.ApplicationCore.Interfaces;
using Finance.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests.Infrastructure.Repositories
{
	public class ValueRepositoryTests
	{
		private readonly IAsyncRepository<Value> _valueRepository;
		private readonly FinanceContext _financeContext;

		public ValueRepositoryTests()
		{
			var dbOptions = new DbContextOptionsBuilder<FinanceContext>()
				.UseInMemoryDatabase(databaseName: "TestFinance")
				.Options;
			_financeContext = new FinanceContext(dbOptions);
			_financeContext.Database.EnsureDeleted(); //clear the in-memory datastore
			_valueRepository = new EFRepository<Value>(_financeContext);
		}

		[Fact]
		public async Task GetByIdTest()
		{
			var existingValue = new Value() { Id = 1, Name = "IntegrationTestValue" };
			_financeContext.Values.Add(existingValue);
			_financeContext.SaveChanges();

			var valueFromRepository = await _valueRepository.GetByIdAsync(existingValue.Id);
			Assert.Equal(existingValue.Id, valueFromRepository.Id);
			Assert.Equal(existingValue.Name, valueFromRepository.Name);
		}

		[Fact]
		public async Task ListAllAsyncTest()
		{
			var existingValue = new Value() { Id = 1, Name = "IntegrationTestValue" };
			_financeContext.Values.Add(existingValue);
			_financeContext.SaveChanges();

			var valueFromRepo = await _valueRepository.ListAllAsync();
			var firstValueFromRepo = valueFromRepo.FirstOrDefault();
			Assert.Equal(existingValue.Id, firstValueFromRepo.Id);
			Assert.Equal(existingValue.Name, firstValueFromRepo.Name);
		}
	}
}
