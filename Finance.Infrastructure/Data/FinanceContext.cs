﻿using Finance.ApplicationCore.Entities.Value;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Finance.Infrastructure.Data
{
	public class FinanceContext : DbContext
	{
		public FinanceContext()
		{

		}

		public FinanceContext(DbContextOptions<FinanceContext> options) : base(options)
		{

		}

		public DbSet<Value> Values { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			builder.Entity<Value>(ConfigureValue);
		}

		private void ConfigureValue(EntityTypeBuilder<Value> builder)
		{
			builder.Property(value => value.Name)
				.IsRequired(true)
				.HasColumnType("varchar(200)");
		}
	}
}
