﻿using Finance.ApplicationCore.Entities.Value;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.Infrastructure.Data
{
	public class FinanceContextSeed
	{
		public static async Task SeedAsync(FinanceContext financeContext)
		{
			try
			{
				if (!financeContext.Values.Any())
				{
					financeContext.Values.AddRange(
						new List<Value>() { new Value() { Name = "Test" } });

					await financeContext.SaveChangesAsync();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		public static void Seed(FinanceContext financeContext)
		{
			SeedAsync(financeContext).RunSynchronously();
		}

		public static async Task RecreateDatabase(FinanceContext financeContext)
		{
			await financeContext.Database.EnsureDeletedAsync();
			await financeContext.Database.EnsureCreatedAsync();
			await financeContext.Database.MigrateAsync();
		}
	}
}
